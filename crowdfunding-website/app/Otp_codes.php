<?php

namespace App;

use App\Traits\UsesUuid;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Otp_codes extends Model
{
    use UsesUuid;
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
