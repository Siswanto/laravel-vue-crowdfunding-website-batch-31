<?php

namespace App;

use App\Traits\UsesUuid;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Otp_codes;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable , UsesUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'role_id', 'photo_profile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function otp_code(){
        return $this->hasOne('App\Otp_codes');
    }

    public function isAdmin(){
        if ($this->role_id === $this->get_role_admin()) {
            return true;
        }

        return false;

        // if ($this->role) {
        //     if ($this->role->name == 'admin') {
        //         return true;
        //     }
        // }

        // return false;
    }

    public function get_role_admin(){
        $role = Role::where('name', 'admin')->first();

        return $role->id;
    }

    public function get_role_user(){
        $role = Role::where('name', 'user')->first();

        return $role->id;
    }

    public static function boot(){
        parent::boot();

        static::creating(function($model){
            $model->role_id = $model->get_role_user();
        });

        static::created(function($model){
            $model->generate_otp_code();
        });
    }

    public function generate_otp_code(){
        do {
            $random = mt_rand(100000, 999999);
            $check = Otp_codes::where('otp', $random)->first();

        } while ($check);

        $now = Carbon::now();

        //Create otp code
        $otp_code = Otp_codes::updateOrCreate(
            ['user_id'  => $this->id],
            ['otp'      => $random, 'valid_until' => $now->addMinutes(5)]
        );

    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function otp_codes(){
        return $this->hasOne('App\Otp_codes', 'user_id', 'id');
    }
}
