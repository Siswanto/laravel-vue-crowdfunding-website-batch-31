<?php

namespace App;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Role extends Model
{
    use UsesUuid;
    
    protected $fillable = ['name'];

    public function users(){
        return $this->hasMany('App\User');
    }
}
