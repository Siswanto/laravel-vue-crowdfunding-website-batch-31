//Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

var daftar_hewan_sort = daftarHewan.sort()
for (let index = 0; index < daftar_hewan_sort.length; index++) {
    console.log(daftar_hewan_sort[index]);
}



//Soal 2
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
function introduce(data){
    return "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby
}
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


//Soal 3
function hitung_huruf_vokal(str){
    var daftarHurufVokal = 'aiueo'
    var hitung = 0

    for(var i = 0; i < str.length; i++){
        if (daftarHurufVokal.indexOf(str.toLowerCase()[i]) !== -1) {
            hitung += 1;
        }
    }
    return hitung;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2


//Soal 4
function hitung(x){
    return 2 * x - 2
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8