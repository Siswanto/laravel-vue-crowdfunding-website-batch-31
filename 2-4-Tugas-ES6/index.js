//Soal 1
const luasPersegiPanjang = (panjang, lebar) => {
    const luas = panjang * lebar
    return luas
}

const kelilingPersegiPanjang = (panjang, lebar) => {
    const keliling = 2 * (panjang + lebar)
    return keliling
}

console.log(kelilingPersegiPanjang(2, 4))
console.log(luasPersegiPanjang(3, 4))

//Soal 2
const newFunction = (firstName, lastname) =>{
    return {
        firstName,
        lastname,
        fullName: () => console.log(`${firstName} ${lastname}`)
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName() 


//Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)


//Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)


//Soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet
var after = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet}`

console.log(after)


