<?php

abstract class Hewan {
  public $nama;
  public $darah = 50;
  public $jumlahKaki;
  public $keahlian;
  abstract public function atraksi();
}

trait Fight {
  public $attackPower;
  public $deffencePower;

  public function serang($lawan){
    echo "{$this->nama} sedang menyerang {$lawan->nama} <br><br>";
    $this->diserang($lawan);
  }

  public function diserang($korban){
    echo "{$korban->nama} sedang diserang <br><br>";
    $korban->darah = $korban->darah - $this->attackPower / $this->deffencePower;
  }
}

class Elang extends Hewan {
  use Fight;
  public function __construct($nama){
    $this->nama = $nama;
    $this->jumlahKaki = 2;
    $this->keahlian = "terbang tinggi";
    $this->attackPower = 10;
    $this->deffencePower = 5;
  }

  public function atraksi(){
    echo "{$this->nama} sedang {$this->keahlian}";
  }

  public function getInfoHewan(){
    echo "
        Nama : {$this->nama}<br>
        Darah : {$this->darah}<br>
        Jumlah kaki : {$this->jumlahKaki}<br>
        Keahlian : {$this->keahlian}<br>
        Jenis Hewan : Elang <br>
        Attack Power : {$this->attackPower} <br>
        Defence Power : {$this->deffencePower}";
  }
}

class Harimau extends Hewan {
  use Fight;
  public function __construct($nama){
    $this->nama = $nama;
    $this->jumlahKaki = 4;
    $this->keahlian = "lari cepat";
    $this->attackPower = 7;
    $this->deffencePower = 8;
  }

  public function atraksi(){
    echo "{$this->nama} sedang {$this->keahlian}";
  }

  public function getInfoHewan(){
    echo "
      Nama : {$this->nama}<br>
      Darah : {$this->darah}<br>
      Jumlah kaki : {$this->jumlahKaki}<br>
      Keahlian : {$this->keahlian}<br>
      Jenis Hewan : Harimau <br>
      Attack Power : {$this->attackPower} <br>
      Defence Power : {$this->deffencePower}";
  }
}

$elang = new Elang('elang_1');
$harimau = new Harimau('harimau_1');
echo $elang->getInfoHewan()."<br><br>";
echo $harimau->getInfoHewan()."<br><br>";
echo $elang->atraksi()."<br><br>";
echo $harimau->atraksi()."<br><br>";
echo $harimau->serang($elang);
echo $elang->getInfoHewan()."<br><br>";
echo $harimau->getInfoHewan()."<br><br>";

?>